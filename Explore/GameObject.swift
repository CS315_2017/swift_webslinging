//
//  GameObject.swift
//  Explore
//
//  Created by Rich Tanner on 10/25/17.
//  Copyright © 2017 CS_315@ACU. All rights reserved.
//

import UIKit

class GameObject: NSObject {
    
    // This object should represent everything you need for your Games
    // You can think of it as an empty template or as a Table in a Database, but 
    // without the need for a "Primary Key".  Each Object is independant of the other Objects
    // and, in fact, each COULD be an exact copy of each other.  Uniqueness may not ever matter
    // until we start counting and sorting them in Arrays and Dictionaries
    
    
    // C315 - Make code change:
    // TODO: Add an Object Property for "gameURL" that you can keep a webpage link in for loading in a UIWebView.  Also, UPDATE the init constructor below to require new property!
    
    
    var gameTitle: String = ""
    var gameImageName: String = ""
    var gameReleaseYear: Int = 0
    var gameDescription: String = ""
    var minPlayers: Int = 0
    var maxPlayers: Int = 0
    var gamePublisher: String = ""
    var gamePlatform: String = ""
    
    init(title: String, gameImage: String, releaseYear: Int, description: String, minPlayer: Int, maxPlayer: Int, publisher: String, platform: String) {
        
        gameTitle = title
        gameImageName = gameImage
        gameReleaseYear = releaseYear
        gameDescription = description
        minPlayers = minPlayer
        maxPlayers = maxPlayer
        gamePublisher = publisher
        gamePlatform = platform
    }

}
