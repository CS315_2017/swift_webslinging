//
//  GameWebDelegateViewController.swift
//  Explore
//
//  Created by Rich Tanner on 11/13/17.
//  Copyright © 2017 Rich Tanner. All rights reserved.
//

import UIKit

class GameWebDelegateViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var spinny: UIActivityIndicatorView!
    @IBOutlet weak var webby: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // show spinny here
        spinny.isHidden = false
        
        // C315 - Make code change:
        // TODO: we should have been given a URL by the Controller that pushed us on... so we should tell our webView (webby) to load that up now
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK - UIWebViewDelegate functions
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        // if we wanted to double check to URL being loaded, we could do that HERE and then in our RETURN, only allow "true" if we want that particualr URL or schema.
        // we can also do all kinds of jinky URL appending, manipulating, and injecting in here... if we are so inclined.
        // for THIS project, we won't do any of that... but we COULD
        return true
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        // maybe show a freindly error message?  ...and
        spinny.isHidden = true
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        // here we UNHIDE the indicator
        spinny.isHidden = false
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        // guess what?  HIDE the indicator
        spinny.isHidden = true
    }
    

}
